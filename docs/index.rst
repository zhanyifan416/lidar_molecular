.. Molecular scattering documentation master file, created by
   sphinx-quickstart on Fri Nov 25 08:19:40 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Molecular scattering's documentation!
================================================

Contents:

.. toctree::
   :maxdepth: 2

   bucholtz
   molecular_properties

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

